# Manjaro-Splash-Blue

![License](https://img.shields.io/gitlab/license/18315535?color=green&style=for-the-badge)

This is my own take on the Manjaro KDE plasma splashscreen.
The color theme of my desktop is blue and I wanted the splashscreen to 
match.

I used the manjaro-logo and loading bar from here: https://store.kde.org/p/1296444/
and simply modified the color to fit the PC theme.

I then added the logo and load bar to this currently existing splashscreen https://store.kde.org/p/1295753/ and replaced the one that comes with the loading bar. Here are a few screenshots:

![SplashScreen](contents/previews/splash.png "Splashscreen")

![Blue Logo](contents/previews/logo.png "Blue Logo")

